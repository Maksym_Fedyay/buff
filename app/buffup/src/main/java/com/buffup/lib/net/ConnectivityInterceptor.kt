package com.buffup.lib.net

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * Connectivity interceptor checks if network connection is live
 * before actual network request invocation.
 *
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class ConnectivityInterceptor(private val context: Context) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!ConnectivityChecker.isConnectingToInternet(context)) {
            throw NoConnectivityException()
        }
        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }

}
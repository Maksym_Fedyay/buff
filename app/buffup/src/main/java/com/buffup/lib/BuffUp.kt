package com.buffup.lib

import android.content.Context
import com.buffup.lib.api.Api

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class BuffUp private constructor(context: Context) {

    val stream: BuffStream = BuffStream(Api(context));

    companion object : SingletonHolder<BuffUp, Context> (::BuffUp)
}
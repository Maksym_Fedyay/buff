package com.buffup.lib

import com.buffup.lib.api.models.Buff

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class BuffStreamData(val buff: Buff?, val buffListener: BuffListener) {

    var isInit = true

    var time: Int? = buff?.result?.time_to_show

    var answer: Buff.Answer? = null

    var isOver = false

    fun tick() : Int?{
        isInit = false
        time = time?.dec()
        return time
    }

}
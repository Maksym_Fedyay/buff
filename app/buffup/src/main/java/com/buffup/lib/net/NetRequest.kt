package com.buffup.lib.net

import androidx.lifecycle.Observer
import com.buffup.lib.net.Res.Companion.error
import com.buffup.lib.net.Res.Companion.loading
import com.buffup.lib.net.Res.Companion.success

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
abstract class NetRequest<IdType, ResponseType> : NetRequestBase<IdType, ResponseType>() {

    override fun execute(): NetRequestBase<IdType, ResponseType> {
        // Prevent request bursting
        if (data.value != null && data.value!!.isLoading) return this
        callId = callId
        performNetRequest()
        return this
    }


    override fun performNetRequest() {
        data.value = loading(callId, null)
        val call = call().setCallId(callId)
        call.enqueue()
        data.addSource(call, Observer {
            if (it?.state != LiveCall.STATE_COMPLETE) return@Observer
            data.removeSource(it)
            if (it.isSuccessful) {
                data.setValue(success(it.callId, it.body))
            } else {
                val netError = NetError(it.code, it.errorMessage)
                onRequestFailed(netError) // Notify about fail
                data.setValue(error(it.callId, null, netError))
            }
        })
    }

}
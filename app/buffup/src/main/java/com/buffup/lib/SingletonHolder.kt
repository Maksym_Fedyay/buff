package com.buffup.lib

/**
 * Trust the Kotlin authors on this: this code is actually borrowed
 * directly from the implementation of the lazy()
 * function in the Kotlin standard library
 *
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
open class SingletonHolder<out T : Any, in A>(creator: (A) -> T) {
    private var creator: ((A) -> T)? = creator
    @Volatile
    private var instance: T? = null

    fun get(arg: A): T {
        val i = instance
        if (i != null) {
            return i
        }

        return synchronized(this) {
            val i2 = instance
            if (i2 != null) {
                i2
            } else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}
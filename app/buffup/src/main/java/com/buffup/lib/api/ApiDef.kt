package com.buffup.lib.api

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
interface ApiDef {

    fun getBuffApi() : BuffApi

}
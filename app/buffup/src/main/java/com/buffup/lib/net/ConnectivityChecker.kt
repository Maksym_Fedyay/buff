package com.buffup.lib.net

import android.content.Context
import android.net.ConnectivityManager
import android.os.Handler
import androidx.lifecycle.LiveData

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
object ConnectivityChecker {
    fun isConnectingToInternet(context: Context): Boolean {
        return if (isOnline(context)) {
            try { // TODO: Implement 3 time ping in case if first ping fails
                val process =
                    Runtime.getRuntime().exec("ping -c 1 www.google.com")
                process.waitFor() == 0
            } catch (e: Exception) {
                false
            }
        } else {
            false
        }
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = connectivityManager.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }
}
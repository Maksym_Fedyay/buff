package com.buffup.lib.api.models

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class Buff {

    var result: Result? = null

    inner class Result {
        var id = 0
        @SerializedName("client_id")
        var clientId = 0
        @SerializedName("stream_id")
        var streamId = 0
        var time_to_show = 0
        var priority = 0
        var created_at: String? = null
        var author: Author? = null
        var question: Question? = null
        var answers = ArrayList<Answer>()
        var language: String? = null
    }

    inner class Question {
        var id = 0
        var title: String? = null
        var category = 0
    }

    inner class Author {
        var first_name: String? = null
        var last_name: String? = null
        var image: String? = null
    }

    inner class Answer {
        var id = 0
        var buff_id = 0
        var title: String? = null
        var image = Image()
    }

    inner class Image {
        @SerializedName("0")
        var small: AnswerImageData? = null
        @SerializedName("1")
        var medium: AnswerImageData? = null
        @SerializedName("2")
        var large: AnswerImageData? = null
    }

    inner class AnswerImageData {
        var id: String? = null
        var key: String? = null
        var url: String? = null
    }
}
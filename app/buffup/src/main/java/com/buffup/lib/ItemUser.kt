package com.buffup.lib

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.buffup.lib.api.models.Buff
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_user.view.*

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class ItemUser(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
    FrameLayout(context, attrs, defStyleAttr) {

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context) : this(context, null, 0)

    init {
        LayoutInflater.from(context).inflate(R.layout.item_user, this)
    }

    @SuppressLint("SetTextI18n")
    fun setUser(user: Buff.Author?){
        if (user != null) {
            if(user.image != null) Picasso.get().load(user.image).into(user_avatar)
            user_name.text = user.first_name + " " + user.last_name
        }
    }
}
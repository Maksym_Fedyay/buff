package com.buffup.lib

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.buffup.lib.api.models.Buff
import kotlinx.android.synthetic.main.item_question.view.*

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class ItemQuestion(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
    LinearLayout(context, attrs, defStyleAttr) {

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context?) : this(context, null, 0)

    init {
        LayoutInflater.from(context).inflate(R.layout.item_question, this)
    }

    fun setQuestion(question: Buff.Question?) {
        view_question.text = question?.title
    }

    fun setTimeCount(secs: Int?){
        item_timer.text = secs?.toString()
    }

}
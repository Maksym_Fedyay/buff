package com.buffup.lib

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.buffup.lib.api.models.Buff
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_answer.view.*

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class ItemAnswer(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
    LinearLayout(context, attrs, defStyleAttr) {

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context?) : this(context, null, 0)

    init {
        LayoutInflater.from(context).inflate(R.layout.item_answer, this)
    }


    var answer: Buff.Answer? = null
        set(value) {
            field = value
            Picasso.get().load(answer?.image?.large?.url).into(answer_icon)
            view_answer.text = answer?.title
        }


//    fun setAnswer(answer: Buff.Answer) {
//        Picasso.get().load(answer.image.large?.url).into(answer_icon)
//        view_answer.text = answer.title
//    }

}
package com.buffup.lib.net

/**
 *
 * @param <I> Id type.
 * @param <T> Data type.
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
</T></I> */
class Res<I, T> private constructor(val status: Status, id: I?, data: T?, netError: NetError?) {

    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    val id: I?
    val data: T?
    val netError: NetError?
    val isLoading: Boolean
        get() = status == Status.LOADING

    companion object {
        @JvmStatic
        fun <I, T> loading(id: I?, data: T?): Res<I, T> {
            return Res(Status.LOADING, id, data, null)
        }

        @JvmStatic
        fun <I, T> success(id: I?, data: T?): Res<I, T> {
            return Res(Status.SUCCESS, id, data, null)
        }

        @JvmStatic
        fun <I, T> error(id: I?, data: T?, netError: NetError?): Res<I, T> {
            return Res(Status.ERROR, id, data, netError)
        }
    }

    init {
        this.id = id
        this.data = data
        this.netError = netError
    }
}
package com.buffup.lib.net

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 **/
abstract class NetRequestBase<IdType, ResponseType> {

    var data = MediatorLiveData<Res<IdType, ResponseType>?>()

    /**
     * Performs network request execution.
     * @return Instance of [NetRequestBase] to chain method invocation.
     */
    @MainThread
    abstract fun execute(): NetRequestBase<IdType, ResponseType>

    /**
     * Performs data operation open remote repository.
     */
    protected abstract fun performNetRequest()

    /**
     * Called to create [LiveCall].
     *
     * @return [LiveCall] which is holds data returned by
     * [retrofit2.Call.enqueue] callback invocation.
     */
    @MainThread
    protected abstract fun call(): LiveCall<IdType, ResponseType>

    /**
     * Called when the fetch fails.
     *
     * @param netError
     */
    @MainThread
    protected fun onRequestFailed(netError: NetError?) {
    }

    /**
     *
     */
    var callId: IdType? = null

    /**
     * Returns a [LiveData] that represents the resource.
     *
     * @return [LiveData]
     */
    fun asLiveData(): LiveData<Res<IdType, ResponseType>?> {
        return data
    }

    fun clear() {
        data.value = null
    }
}
package com.buffup.lib.api

import android.content.Context
import com.buffup.lib.net.ConnectivityInterceptor
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
internal class Api constructor(context: Context) : ApiDef {

    private val API_URL = "https://buffup.proxy.beeceptor.com"

    private val buffApi: BuffApi

    init {

        val builder = OkHttpClient.Builder()
        builder.addInterceptor(ConnectivityInterceptor(context))

//        // Add logging
//        builder.addInterceptor(HttpLoggingInterceptor()
//            .apply { level = HttpLoggingInterceptor.Level.BODY })


        val okHttpClient = builder.build();

        val gson = GsonBuilder().setLenient().create()

        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()

        this.buffApi = retrofit.create(BuffApi::class.java)
    }


    override fun getBuffApi(): BuffApi {
        return this.buffApi
    }

}
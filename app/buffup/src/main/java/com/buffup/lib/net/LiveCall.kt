package com.buffup.lib.net

import android.util.Log
import androidx.annotation.IntDef
import androidx.lifecycle.LiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

/**
 *
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class LiveCall<K, T>(call: Call<T>) : LiveData<LiveCall<K, T>?>() {

    private val TAG: String = LiveCall::class.java.simpleName

    /**
     * Retrofit Call instance
     */
    private val call: Call<T>?

    /**
     * Creates new instance of [LiveCall].
     * Instance can be used only once.
     */
    init {
        this.call = call
    }

    /**
     * [LiveCall] execution state.
     * <br></br>
     * - [LiveCall.STATE_EXECUTING]<br>
     * - [LiveCall.STATE_COMPLETE]<br>
     * - [LiveCall.STATE_IDLE]
     */
    @IntDef(value = [STATE_COMPLETE, STATE_EXECUTING, STATE_IDLE])
    internal annotation class State


    companion object {
        /**
         * Indicates that [LiveCall] is in idle state.
         * Instantiated and [LiveCall.enqueue] method have
         * never been called.
         */
        const val STATE_IDLE = -1
        /**
         * Indicates that [Call] was [enqueued][LiveCall.enqueue]
         * and currently executing.
         */
        const val STATE_EXECUTING = 0
        /**
         * Indicates that [Call] was [enqueued][LiveCall.enqueue]
         * and completed.
         */
        const val STATE_COMPLETE = 1
    }


    @get:State
    @State
    var state = STATE_IDLE
        private set

    /**
     * Current HTTP status code.
     */
    var code = -1
        private set

    /**
     * Current error message.
     */
    var errorMessage: String? = null
        private set

    /**
     * Current response body.
     */
    var body: T? = null
        private set

    /**
     * Call id that was supplied
     */
    var callId: K? = null
        private set

    /**
     * Enqueues [Call].
     *
     * @return Parent instance.
     */
    fun enqueue(): LiveCall<K, T> {
        if (state == STATE_EXECUTING) return this
        if (state == STATE_COMPLETE) {
            Log.i(
                TAG,
                "LiveCall can be executed only once.",
                Throwable()
            )
            return this
        }
        if (call == null) { // Just in case
            Log.i(
                TAG, "LiveCall invalid state. Instantiated with null Call param",
                Throwable()
            )
            return this
        }
        state = STATE_EXECUTING
        value = this
        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                code = response.code()
                if (response.isSuccessful) {
                    body = response.body()
                    errorMessage = null
                } else {
                    if (null != response.errorBody()) {
                        var message: String? = null
                        try {
                            message = response.errorBody()!!.string()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        if (message == null || message.trim { it <= ' ' }.isEmpty()) {
                            message = response.message()
                        }
                        errorMessage = message
                        body = null
                    } else {
                        errorMessage = "Unknown error."
                        body = null
                    }
                }
                state = STATE_COMPLETE
                value = this@LiveCall
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                code = if (t is NoConnectivityException) {
                    NoConnectivityException.CODE
                } else {
                    0
                }
                errorMessage = t.message
                body = null
                state = STATE_COMPLETE
                value = this@LiveCall
            }
        })
        return this
    }

    /**
     * Checks success state of executed [Call].
     *
     * @return `true` if call executed successfully,
     * `false` otherwise.
     */
    val isSuccessful: Boolean
        get() = code in 200..299

    /**
     * @param callId
     * @return
     */
    fun setCallId(callId: K?): LiveCall<K, T> {
        if (state != STATE_IDLE) return this // Once LiveCall enqueued callId can't be changed/
        this.callId = callId
        return this
    }

    var isCanceled = false
        private set

    fun cancel() {
        if (isCanceled) return
        isCanceled = true
        state = STATE_COMPLETE
        call!!.cancel()
        value = this
    }
}
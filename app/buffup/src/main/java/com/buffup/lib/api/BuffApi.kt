package com.buffup.lib.api

import com.buffup.lib.api.models.Buff
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
interface BuffApi {

    @GET("/buffs/{id}")
    fun getBuff(@Path("id") id: Int) : Call<Buff>

}
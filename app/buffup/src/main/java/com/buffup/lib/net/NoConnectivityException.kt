package com.buffup.lib.net

import java.io.IOException

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class NoConnectivityException : IOException() {
    companion object {
        const val CODE = -1000
        const val message = "No connectivity exception"
    }
}
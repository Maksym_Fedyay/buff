package com.buffup.lib

import com.buffup.lib.api.models.Buff

/**
 * BuffListener must be used as notification channel
 * between [Buff] and [BuffStream], to avoid library user
 * to deal with the [BuffUpVoteView] events.
 *
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
interface BuffListener {

    /**
     * Must be called when selected particular buff answer.
     */
    fun onAnswer()

    /**
     * Must be called when buff explicitly closed.
     */
    fun onClose()

    /**
     * Must be called when UI that represent Buff
     * was effectively closed. (i.e. close animation is over)
     */
    fun onClosed()
}
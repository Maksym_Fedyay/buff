package com.buffup.lib.net

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class NetError(val errorCode: Int, val message: String?) {
    companion object {
        const val NOT_AUTHORIZED = 401
        const val CONNECTIVITY_ERROR = NoConnectivityException.CODE
    }
}
package com.buffup.lib

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.buffup.lib.api.Api
import com.buffup.lib.api.ApiDef
import com.buffup.lib.api.models.Buff
import com.buffup.lib.net.LiveCall
import com.buffup.lib.net.NetRequest
import com.buffup.lib.net.Res
import retrofit2.Call
import java.util.*
import javax.inject.Inject


/**
 * Manages [Buff]s.
 *
 * To start receiving [Buff]s call [startBuffStream]
 * and observe LivaData returned by [getBuffStream]
 *
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class BuffStream(api: ApiDef) : BuffListener {


    private companion object{
        const val BUFF_POLL_PERIOD = 30000L; // Mills
        const val AFTER_ANSWER_PERIOD = 2000L; // Mills
    }

    /**
     * Handler to execute periodic [Buff] fetching.
     */
    private val buffStreamHandler: Handler = Handler(Looper.getMainLooper())

    /**
     * [Api] instance
     */
    private val api: ApiDef = api

    /**
     * LivaData that will be exposed to User.
     */
    private val mediatorLiveData: MediatorLiveData<BuffStreamData> = MediatorLiveData()

    /**
     * Queue of fetched [Buff]s.
     */
    private val queue: Queue<BuffStreamData> = LinkedList<BuffStreamData>()

    /**
     * Provide [Buff] stream over observable [LiveData].
     */
    @MainThread
    fun getBuffStream(): LiveData<BuffStreamData> {
        return mediatorLiveData
    }


    /**
     * Buff that is "On Air".
     */
    private var buffStreamData: BuffStreamData? = null


    /**
     *
     */
    private fun getBuffStream(id: Int) {
        val ld: LiveData<Res<Int, Buff>?> = object : NetRequest<Int, Buff>() {
            override fun call(): LiveCall<Int, Buff> {
                val call: Call<Buff> = api.getBuffApi().getBuff(id)
                return LiveCall(call)
            }
        }.execute().asLiveData()

        mediatorLiveData.addSource(ld, androidx.lifecycle.Observer {
            if (null == it) return@Observer
            when (it.status) {
                Res.Status.SUCCESS -> {
                    mediatorLiveData.removeSource(ld)
                    val buffStreamData = BuffStreamData(it.data, this@BuffStream)
                    if (null != this.buffStreamData) {
                        queue.offer(buffStreamData)
                    } else {
                        this.buffStreamData = buffStreamData
                        mediatorLiveData.value = buffStreamData
                        startTimer()
                    }

                }
                Res.Status.LOADING -> {
                    // No need to show progress, yet
                }
                Res.Status.ERROR -> {
                    mediatorLiveData.removeSource(ld)
                    // If failed to get buff, silently ignore it
                }
            }
        })
    }


    /**
     * Current buff id.
     */
    private var buffId = 0


    /**
     * Flag that indicates if [Buff] stream is started.
     */
    private var isBuffStreamStarted: Boolean = false


    /**
     * Starts [Buff] stream.
     */
    @MainThread
    fun startBuffStream() {
        if (isBuffStreamStarted) return // Ignore if already started
        isBuffStreamStarted = true
        buffStreamHandler.post(buffStreamTask)
    }


    /**
     * Buff stream task.
     */
    private val buffStreamTask = object : Runnable {
        override fun run() {
            if (!isBuffStreamStarted) {
                // TODO: nullify all stream related structures
                return
            }
            if (++buffId > 5) buffId = 1
            getBuffStream(buffId)
            buffStreamHandler.postDelayed(this, BUFF_POLL_PERIOD)
        }
    }


    /**
     * Stops [Buff] stream.
     */
    @MainThread
    fun stopBuffStream() {
        isBuffStreamStarted = false
    }


    /* Handle buff events */

    override fun onAnswer() {
        val answer: Buff.Answer? = buffStreamData?.answer
        clearTimer()
        timerHandler.postDelayed({
            buffStreamData?.isOver = true
            mediatorLiveData.value = buffStreamData
        }, AFTER_ANSWER_PERIOD)
    }


    override fun onClose() {
        clearTimer()
        buffStreamData?.isOver = true
        mediatorLiveData.value = buffStreamData
    }


    override fun onClosed() {
        nextBuff()
    }


    /**
     * Push next Buff if any in the queue.
     */
    private fun nextBuff() {
        buffStreamData = queue.poll()
        mediatorLiveData.value = buffStreamData
        startTimer()
    }


    /* Timer implementation */


    private val timerHandler = Handler(Looper.getMainLooper())


    private val timerTask = object : Runnable {
        override fun run() {
            val secs = buffStreamData?.tick()
            if (secs != null && secs >= 1) {
                mediatorLiveData.value = buffStreamData
                timerHandler.postDelayed(this, 1000)
            } else {
                buffStreamData?.isOver = true
                mediatorLiveData.value = buffStreamData
                clearTimer()
            }
        }
    }


    /**
     * Indicates if timer is started ot not.
     */
    private var isTimerStarted = false


    /**
     * Starts count down timer.
     */
    private fun startTimer() {
        if (isTimerStarted) return
        isTimerStarted = true
        timerHandler.postDelayed(timerTask, 1000)
    }

    /**
     * Stops timer and removes all pending tasks.
     */
    private fun clearTimer() {
        timerHandler.removeCallbacksAndMessages(null)
        isTimerStarted = false
    }

}
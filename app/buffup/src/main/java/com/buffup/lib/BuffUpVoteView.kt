package com.buffup.lib

import android.animation.Animator
import android.animation.Animator.AnimatorListener
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import com.buffup.lib.api.models.Buff
import kotlinx.android.synthetic.main.buffup_vote_view.view.*

/**
 * BuffUpVoteView implements UI for displaying Buff data:
 * - Question author,
 * - Question,
 * - Group of answers.
 *
 * To display [Buff]s, supply instance of BuffUpVoteView
 * with the result obtained from [com.buffup.lib.BuffStream.getBuffStream].
 *
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class BuffUpVoteView(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
    FrameLayout(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)

    init {
        LayoutInflater.from(context).inflate(R.layout.buffup_vote_view, this)
        btn_close.setOnClickListener { buffStreamData?.buffListener?.onClose() }
    }

    private var buffStreamData: BuffStreamData? = null

    private var isInit = true

    /**
     * Sets [BuffStreamData] to be displayed.
     */
    fun setBuff(buffStreamData: BuffStreamData?) {
        this.buffStreamData = buffStreamData

        if (buffStreamData == null || buffStreamData.isOver) { // Don't show if time is out
            hide()
            return
        }

        item_question.setTimeCount(buffStreamData.time)

        if(isInit || buffStreamData.isInit){
            item_user.setUser(buffStreamData.buff?.result?.author)
            item_question.setQuestion(buffStreamData.buff?.result?.question)
            addAnswers(buffStreamData.buff?.result?.answers)
            if(buffStreamData.isInit){
                viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                    override fun onPreDraw(): Boolean {
                        viewTreeObserver.removeOnPreDrawListener(this)
                        show()
                        return false
                    }
                })
            } else {
                root_view.visibility = View.VISIBLE
            }
            isInit = false
        }
    }


    /**
     * Adds group af answer to the View.
     */
    private fun addAnswers(answers: List<Buff.Answer>?) {
        answers_container.removeAllViews()
        if (answers != null) for (answer in answers) addAnswer(answer)
    }


    /**
     * Creates [ItemAnswer] view to display answer data,
     * and attach it to the answers container.
     */
    private fun addAnswer(answer: Buff.Answer) {
        val itemAnswer = ItemAnswer(context)
        val lp = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        itemAnswer.layoutParams = lp
        itemAnswer.answer = answer
        itemAnswer.setOnClickListener {
            buffStreamData?.answer = (it as ItemAnswer).answer
            buffStreamData?.buffListener?.onAnswer()
        }
        answers_container.addView(itemAnswer)
    }


    /**
     * Shows up UI.
     * This is the place to create show/in animation.
     */
    private fun show() {
        val animSet = AnimatorSet()
        val transX: ObjectAnimator =
            ObjectAnimator.ofFloat(this, "TranslationX", -width.toFloat(), 0f)
        animSet.play(transX)
        root_view.visibility = View.VISIBLE
        animSet.start()
    }


    /**
     * Hides UI. This is the place to create hide/out animation.
     */
    private fun hide() {
        val animSet = AnimatorSet()
        val transX: ObjectAnimator =
            ObjectAnimator.ofFloat(this, "TranslationX", 0f, -width.toFloat())
        animSet.play(transX)
        animSet.addListener(object : AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {}
            override fun onAnimationEnd(animation: Animator?) {
                root_view.visibility = View.INVISIBLE
                buffStreamData?.buffListener?.onClosed()
            }
            override fun onAnimationCancel(animation: Animator?) {}
            override fun onAnimationStart(animation: Animator?) {}
        })
        animSet.start()
    }

}
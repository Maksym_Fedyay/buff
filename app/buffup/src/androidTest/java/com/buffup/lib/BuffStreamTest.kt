package com.buffup.lib

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.platform.app.InstrumentationRegistry
import com.buffup.lib.api.ApiDef
import com.buffup.lib.api.BuffApi
import com.buffup.lib.api.models.Buff
import okhttp3.Request
import org.junit.Rule
import org.junit.Test
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Maksym Fedyay on 4/6/20 (mcsimf@gmail.com).
 */
class BuffStreamTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val timeToShow = 15

    @Test
    fun test() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext

        val buff = Buff().also {
            it.result = it.Result()
            it.result!!.time_to_show = timeToShow
        }

        val buffApi = object : BuffApi {
            override fun getBuff(id: Int): Call<Buff> {
                return object : Call<Buff> {
                    override fun enqueue(callback: Callback<Buff>) {
                        callback.onResponse(this, execute())
                    }

                    override fun isExecuted(): Boolean {
                        return false
                    }

                    override fun clone(): Call<Buff> {
                        return this
                    }

                    override fun isCanceled(): Boolean {
                        return false
                    }

                    override fun cancel() {

                    }

                    override fun execute(): Response<Buff> {
                        return Response.success(200, buff)
                    }

                    override fun request(): Request? {
                        return null
                    }
                }
            }
        }

        val apiDef = object : ApiDef {
            override fun getBuffApi(): BuffApi {
                return buffApi
            }
        }

        val stream = BuffStream(apiDef)

        stream.startBuffStream()

        stream.getBuffStream().observeForever {
            process(it)
        }

    }


    private var counter = 0


    private fun process(bsd: BuffStreamData) {
        // Test whatever You need to verify that BuffStream is not violate init data
//        counter++
//        println("frame number = $counter")
//        assert(counter <= timeToShow)
    }


}
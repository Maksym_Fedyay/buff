package com.buffup.app

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    private lateinit var viewModel: MainViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /* This is all about the Buff management in activity */
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.getBuffStream().observe(this, Observer { buff_up_vote_view.setBuff(it) })
        viewModel.startBuffStream()

        // Start video for demo
        setVideoResource("https://buffup-public.s3.eu-west-2.amazonaws.com/video/toronto+nba+cut+3.mp4")
    }


    private var player: SimpleExoPlayer? = null


    fun setVideoResource(videoUrl: String?) {
        player = SimpleExoPlayer.Builder(this).build()

        video_player.player = player

        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(
            this, Util.getUserAgent(this, "yourApplicationName")
        )

        val videoSource: MediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(videoUrl))

        player!!.prepare(videoSource)
        player!!.playWhenReady = true
    }


    override fun onDestroy() {
        super.onDestroy()
        if (!isChangingConfigurations) {
            if (null != player) player!!.release()
        }
    }

}

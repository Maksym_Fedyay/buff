package com.buffup.app

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.buffup.lib.BuffStreamData
import com.buffup.lib.BuffUp

/**
 * @author Maksym Fedyay on 4/4/20 (mcsimf@gmail.com).
 */
class MainViewModel(private val app: Application) : AndroidViewModel(app) {

    fun startBuffStream(){
        BuffUp.get(app).stream.startBuffStream()
    }

    fun getBuffStream(): LiveData<BuffStreamData> {
        return BuffUp.get(app).stream.getBuffStream()
    }

    fun stopBuffStream(){
        BuffUp.get(app).stream.stopBuffStream()
    }

}